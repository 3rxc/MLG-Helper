package de.keepflawless.mlghelper;

import net.labymod.api.LabyModAddon;
import net.labymod.settings.elements.SettingsElement;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.util.MovingObjectPosition;

import java.util.HashMap;
import java.util.List;

public class MLGHelper extends LabyModAddon {

    public static HashMap<Integer,String> Height = new HashMap();


    @Override
    public void onEnable() {
        loadHeights();
        getApi().registerModule(new MLGHelperModule());
    }

    @Override
    public void onDisable() {

    }

    @Override
    public void loadConfig() {

    }

    @Override
    protected void fillSettings(List<SettingsElement> list) {

    }

    public static String calculateAction(int up, int down, int blocksUp) {

        int difference = up - down;

        if(difference < 0) {
            difference = down - up;
        }
        difference += blocksUp;
        if(difference <= 5) {
            return "Kein Fallschaden";
        }
        return Height.get(difference);
    }

    public static void loadHeights() {

        String run = "Laufen";
        String jump = "Springen";
        String impossible = "Unmoeglich";

        Height.put(Integer.valueOf(6), run);
        Height.put(Integer.valueOf(7), run);
        Height.put(Integer.valueOf(8), run);
        Height.put(Integer.valueOf(9), run);
        Height.put(Integer.valueOf(10), run);
        Height.put(Integer.valueOf(11), run);
        Height.put(Integer.valueOf(12), run);
        Height.put(Integer.valueOf(13), run);
        Height.put(Integer.valueOf(14), run);
        Height.put(Integer.valueOf(15), run);
        Height.put(Integer.valueOf(16), run);
        Height.put(Integer.valueOf(17), jump);
        Height.put(Integer.valueOf(18), run);
        Height.put(Integer.valueOf(19), run);
        Height.put(Integer.valueOf(20), jump);
        Height.put(Integer.valueOf(21), run);
        Height.put(Integer.valueOf(22), run);
        Height.put(Integer.valueOf(23), jump);
        Height.put(Integer.valueOf(24), run);
        Height.put(Integer.valueOf(25), impossible);
        Height.put(Integer.valueOf(26), run);
        Height.put(Integer.valueOf(27), run);
        Height.put(Integer.valueOf(28), jump);
        Height.put(Integer.valueOf(29), run);
        Height.put(Integer.valueOf(30), jump);
        Height.put(Integer.valueOf(31), jump);
        Height.put(Integer.valueOf(32), impossible);
        Height.put(Integer.valueOf(33), run);
        Height.put(Integer.valueOf(34), run);
        Height.put(Integer.valueOf(35), jump);
        Height.put(Integer.valueOf(36), run);
        Height.put(Integer.valueOf(37), jump);
        Height.put(Integer.valueOf(38), run);
        Height.put(Integer.valueOf(39), jump);
        Height.put(Integer.valueOf(40), run);
        Height.put(Integer.valueOf(41), jump);
        Height.put(Integer.valueOf(42), run);
        Height.put(Integer.valueOf(43), jump);
        Height.put(Integer.valueOf(44), run);
        Height.put(Integer.valueOf(45), jump);
        Height.put(Integer.valueOf(46), impossible);
        Height.put(Integer.valueOf(47), run);
        Height.put(Integer.valueOf(48), impossible);
        Height.put(Integer.valueOf(49), run);
        Height.put(Integer.valueOf(50), jump);
        Height.put(Integer.valueOf(51), run);
        Height.put(Integer.valueOf(52), jump);
        Height.put(Integer.valueOf(53), run);
        Height.put(Integer.valueOf(54), jump);
        Height.put(Integer.valueOf(55), run);
        Height.put(Integer.valueOf(56), jump);
        Height.put(Integer.valueOf(57), impossible);
        Height.put(Integer.valueOf(58), run);
        Height.put(Integer.valueOf(59), jump);
        Height.put(Integer.valueOf(60), run);
        Height.put(Integer.valueOf(61), jump);
        Height.put(Integer.valueOf(62), run);
        Height.put(Integer.valueOf(63), jump);
        Height.put(Integer.valueOf(64), impossible);
        Height.put(Integer.valueOf(65), run);
        Height.put(Integer.valueOf(66), jump);
        Height.put(Integer.valueOf(67), run);
        Height.put(Integer.valueOf(68), jump);
        Height.put(Integer.valueOf(69), run);
        Height.put(Integer.valueOf(70), impossible);
        Height.put(Integer.valueOf(71), jump);
        Height.put(Integer.valueOf(72), run);
        Height.put(Integer.valueOf(73), jump);
        Height.put(Integer.valueOf(74), run);
        Height.put(Integer.valueOf(75), impossible);
        Height.put(Integer.valueOf(76), jump);
        Height.put(Integer.valueOf(77), run);
        Height.put(Integer.valueOf(78), jump);
        Height.put(Integer.valueOf(79), run);
        Height.put(Integer.valueOf(80), impossible);
        Height.put(Integer.valueOf(81), jump);
        Height.put(Integer.valueOf(82), run);
        Height.put(Integer.valueOf(83), jump);
        Height.put(Integer.valueOf(84), impossible);
        Height.put(Integer.valueOf(85), run);
        Height.put(Integer.valueOf(86), jump);
        Height.put(Integer.valueOf(87), run);
        Height.put(Integer.valueOf(88), impossible);
        Height.put(Integer.valueOf(89), jump);
        Height.put(Integer.valueOf(90), run);
        Height.put(Integer.valueOf(91), jump);
        Height.put(Integer.valueOf(92), impossible);
        Height.put(Integer.valueOf(93), run);
        Height.put(Integer.valueOf(94), jump);
        Height.put(Integer.valueOf(95), run);
        Height.put(Integer.valueOf(96), impossible);
        Height.put(Integer.valueOf(97), jump);
        Height.put(Integer.valueOf(98), run);
        Height.put(Integer.valueOf(99), impossible);
        Height.put(Integer.valueOf(100), jump);
        Height.put(Integer.valueOf(101), run);
        Height.put(Integer.valueOf(102), jump);
        Height.put(Integer.valueOf(103), impossible);
        Height.put(Integer.valueOf(104), run);
        Height.put(Integer.valueOf(105), jump);
        Height.put(Integer.valueOf(106), run);
        Height.put(Integer.valueOf(107), impossible);
        Height.put(Integer.valueOf(108), jump);
        Height.put(Integer.valueOf(109), run);
        Height.put(Integer.valueOf(110), impossible);
        Height.put(Integer.valueOf(111), jump);
        Height.put(Integer.valueOf(112), run);
        Height.put(Integer.valueOf(113), impossible);
        Height.put(Integer.valueOf(114), jump);
        Height.put(Integer.valueOf(115), run);
        Height.put(Integer.valueOf(116), impossible);
        Height.put(Integer.valueOf(117), impossible);
        Height.put(Integer.valueOf(118), run);
        Height.put(Integer.valueOf(119), jump);
        Height.put(Integer.valueOf(120), impossible);
        Height.put(Integer.valueOf(121), run);
        Height.put(Integer.valueOf(122), jump);
        Height.put(Integer.valueOf(123), impossible);
        Height.put(Integer.valueOf(124), run);
        Height.put(Integer.valueOf(125), jump);
        Height.put(Integer.valueOf(126), impossible);
        Height.put(Integer.valueOf(127), run);
        Height.put(Integer.valueOf(128), jump);
        Height.put(Integer.valueOf(129), impossible);
        Height.put(Integer.valueOf(130), run);
        Height.put(Integer.valueOf(131), jump);
        Height.put(Integer.valueOf(132), impossible);
        Height.put(Integer.valueOf(133), impossible);
        Height.put(Integer.valueOf(134), jump);
        Height.put(Integer.valueOf(135), run);
        Height.put(Integer.valueOf(136), impossible);
        Height.put(Integer.valueOf(137), jump);
        Height.put(Integer.valueOf(138), run);
        Height.put(Integer.valueOf(139), impossible);
        Height.put(Integer.valueOf(140), jump);
        Height.put(Integer.valueOf(141), impossible);
        Height.put(Integer.valueOf(142), run);
        Height.put(Integer.valueOf(143), jump);
        Height.put(Integer.valueOf(144), impossible);
        Height.put(Integer.valueOf(145), run);
        Height.put(Integer.valueOf(146), jump);
        Height.put(Integer.valueOf(147), impossible);
        Height.put(Integer.valueOf(148), run);
        Height.put(Integer.valueOf(149), impossible);
        Height.put(Integer.valueOf(150), jump);
        Height.put(Integer.valueOf(151), run);
        Height.put(Integer.valueOf(152), impossible);
        Height.put(Integer.valueOf(153), run);
        Height.put(Integer.valueOf(154), run);
        Height.put(Integer.valueOf(155), impossible);
        Height.put(Integer.valueOf(156), jump);
    }

    public static String returnInformation() {
        Minecraft mc = Minecraft.getMinecraft();
        EntityPlayerSP player = mc.thePlayer;

        MovingObjectPosition object = player.rayTrace(200.0D, 1.0F);


        if(!(object != null) && (object.typeOfHit == MovingObjectPosition.MovingObjectType.BLOCK)) {
            return "Keine Angabe";
        }
        int down = object.getBlockPos().getY() +1;
        int up = (int)player.posY;
        int p = 0;
        if(mc.theWorld.getBlockState(object.getBlockPos()) != Block.getStateById(30)) {
            if(down > up) {
                return "Keine Angabe";
            }
        }
        return calculateAction(up, down, p);
    }
}
